# If you wanna build the image from source, read it

openEuler is an operating system designed for server, and thus in lack of a series of packages in its remote repo. You should build the following packages from source:

    glibc >= 2.33
    qemu >= 5.2.0
    supervisord

and replace the image in `configs/linux-lab/Dockerfile` to use what you build.
